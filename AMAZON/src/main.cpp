#include <stdio.h>

  float distanciaKM;
  int CantProductos;
  float peso;
  float pesoT = 0;
  char destino[13];
  int i=1;

int main() 
{
  //INGRESAMOS DESTINO Y DISTANCIO AL DESTINO
    printf("Ingrese la distancia en la que se encuentra el destino: ");
    scanf("%f", &distanciaKM);
    printf("Ingrese el nombre del destino: ");
    scanf("%s", &destino);
  
  printf("\nRecuerde que el peso minimo del producto es 0.5kg y el maximo es de 3kg\n");
  if (distanciaKM <= 500) //DISTANCIA MENOR A 500km
  {
    for (CantProductos = 0; CantProductos < 5; CantProductos++) 
    {
        printf("Ingrese el peso del producto numero %d: ", i);
        scanf("%f", &peso);

        if (3 >= peso && peso >= 0.5)
        {
            pesoT = peso + pesoT;
            i++;
            printf("Peso del producto dentro del rango aceptado\n");
        }
        else 
        {
            printf("El peso del producto ingresado no entra en el rango de lo permitido ingrese otro producto\n");
            CantProductos--;
        }
    }
  }
  else if (distanciaKM > 500)
  {
        while(pesoT <= 15 && CantProductos < 10)
        {
            printf("Ingrese el peso del producto numero %d: ", i);
            scanf("%f", &peso);

            if (3 >= peso && peso >= 0.5)
            {
                pesoT = peso + pesoT;
                CantProductos++;
                i++;
                printf("Peso del producto dentro del rango aceptado\n");
            }
            else 
            {
                printf("El peso del producto ingresado no entra en el rango de lo permitido ingrese otro producto\n");
            }
        }
        if (pesoT > 15) //podria meter este if dentro del while para que se pueda ingresar otro producto en cambio de este
        {
                pesoT= pesoT - peso;
                printf("AVISO: Con el ultimo producto se superó el limite de peso, por favor ingreselo en el proximo paquete\n");
                CantProductos--;
        }
   }
    printf("El destino del paquete es %s\n", destino);
    printf("La distancia al destino es de %f Km\n", distanciaKM);
    printf("El peso total del paquete es %f Kg\n", pesoT);
    printf("Enviará %d productos", CantProductos);
}